# Bouquet DSL

My version of the DSL created during [Sandstorm's Groovy-DSL tutorial][1].

Gone wild and abstracted some things.

[1]: https://sandstorm.de/de/blog/post/how-to-create-a-dsl-with-groovy.html
