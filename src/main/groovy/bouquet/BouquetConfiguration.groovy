package bouquet;

import dsl.*;

class BouquetConfiguration extends ArrayList<String> {

    int howMany(String flower){
        return this.count { it == flower }
    }
    
    String toString(){
        return "Flowers: " + super.toString();
    } 

}
