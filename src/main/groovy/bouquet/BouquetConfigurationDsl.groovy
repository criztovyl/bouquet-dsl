package bouquet;

import dsl.*;

class BouquetConfigurationDsl extends BouquetConfiguration implements StringCreateable<BouquetConfiguration> {

    public BouquetConfigurationDsl(){
        super()
    }

    void flower(String flower){
        this << flower;
    }

    BouquetConfiguration getDelegateInstance(){ return this; }
}
