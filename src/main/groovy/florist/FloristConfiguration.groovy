package florist;

import bouquet.*;
import dsl.*;

class FloristConfiguration extends HashMap<String, BouquetConfiguration> {

    String toString(){
        return "Florist: " + super.toString();
    }

}
