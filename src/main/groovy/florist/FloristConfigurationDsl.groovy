package florist;

import bouquet.*;
import dsl.*;

class FloristConfigurationDsl extends FloristConfiguration implements StringCreateable<FloristConfiguration> {

    BouquetConfiguration bouquet(String name, Closure script){

        def bouquet = this[name];

        if(bouquet == null) bouquet = this[name] = new BouquetConfigurationDsl();


        bouquet.create(script);

        println "bouqoet: $bouquet";

        return bouquet;

    }

    FloristConfiguration getDelegateInstance(){ return this; }

}
