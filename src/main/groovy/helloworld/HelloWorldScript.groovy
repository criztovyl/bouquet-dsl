public class HelloWorldScript {
    
    public static void execute(Closure script){
        script.delegate = new HelloWorldService();
        script.resolveStrategy = Closure.DELEGATE_FIRST;
        script();
    }
}
