package main;

//import bouquet.*;
import florist.*;

println "Hello, World!"

//def bouqet = new BouquetConfiguration().create """
//flower "corn"
//flower "corn"
//flower "poppy"
//flower "calendula"
//"""
//
//println bouqet.toString()

def florist = new FloristConfigurationDsl().create """
bouquet "meadow", {
    flower "cron";
    flower "corn";
    flower "poppy";
    flower "calendula";
}

bouquet "roses", {
    flower "rose";
    flower "rose";
    flower "rose";
    flower "rose";
}

bouquet "meadow", {
    flower "calendula"
}
"""

println florist.toString();
